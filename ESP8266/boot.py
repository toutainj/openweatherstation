# This file is executed on every boot (including wake-boot from deepsleep)
import esp
#esp.osdebug(None)
esp.osdebug(1)
import uos, machine, time, sys
#uos.dupterm(None, 1) # disable REPL on UART(0)
import gc
#import webrepl
#webrepl.start()
gc.collect()
#gc.enable()
print("boot started")
import network_autoconnect
network_autoconnect.connect()
time.sleep(5)
import ntptime
#ntptime.settime()
print("boot finished")
#from main import main_loop

#import main

#from main import report_error, load_config, get_weather
from main import runtheloop
runtheloop()
#print("boot killed")


