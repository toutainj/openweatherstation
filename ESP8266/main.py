# -*- coding: utf-8 -*-
import micropython
import network
import ntptime
import machine
import time
import ujson
import urequests as requests
from utime import localtime
import ping
# Sonde température/humidite
#import si7021
from si7021 import get_measures
# LCD1306
import ssd1306

from time import sleep_ms, ticks_ms


import utime
import gc
import urequests as requests

print("CPU freq="+str(machine.freq()))

wait_time = 10

def load_config():
    fh = open('config.json', 'r')
    return ujson.loads(fh.read())

def write_to_ssd1306():
    i2c = machine.I2C(scl=machine.Pin(2), sda=machine.Pin(0))
    oled = ssd1306.SSD1306_I2C(128, 64, i2c, 0x3c)
    return oled

def printtime():
    heure = localtime()[3]+1
    if heure < 10:
        heure = str("0")+str(localtime()[3]+1)
    minute = localtime()[4]
    if minute < 10:
        minute = str("0")+str(localtime()[4])
    seconde = localtime()[5]
    if seconde < 10:
        seconde = str("0")+str(localtime()[5])
    jour = localtime()[2]
    if jour < 10:
        jour = str("0")+str(localtime()[2])
    mois = localtime()[1]
    if mois < 10:
        mois = str("0")+str(localtime()[1])
    #else:
    #    mois = localtime()[1]
    annee = localtime()[0]
    
    output = {"time":str(heure)+":"+str(minute)+":"+str(seconde), "date":str(jour)+"/"+str(mois)+"/"+str(annee)}
    return output

def runtheloop(wait_time=wait_time):
    config = load_config()
    
    wifi = config['wifi']['ssid']
    
    proto = config['api']['proto']
    api_host = config['api']['host']
    port = config['api']['port']
    url = config['api']['url']
    heartbeat = config['api']['heartbeat']
    post = config['api']['post']
    api_key = config['api']['key']
    heartbeat_url = proto + api_host + ":" + port + url + heartbeat
    api_url = proto + api_host + ":" + port + url + post
    
    station = network.WLAN(network.STA_IF)
    IP = station.ifconfig()[0]
    oled = write_to_ssd1306()
    oled.fill(0)
    oled.text("Hello Master! =)", 0, 0)
    oled.text("Please wait", 0, 10)
    oled.text("Measure in ", 0, 20)
    oled.text("progress...", 0, 30)
    oled.show()
    while True:
        print("start main loop at "+str(printtime()['time']))
        data = get_measures(mode="all", measures=5, interval=1)
        #ping_api = ping.ping(api_host, count=1, timeout=3000)
        #print(ping_api)
        #if ping_api[1] == 1:
        #    api_ping = "OK"
        #else:
        #    api_ping = "!!"
        try:
            r = requests.get(url=heartbeat_url)
            
            if r.status_code == 200:
                status_api = "OK"
            else:
                status_api = "!!"
        except:
            status_api = "!!"
        #r = requests.get(url=heartbeat_url)
        #print(r.status_code)
        oled.fill(0)
        oled.text("Date  "+str(printtime()['date']), 0, 0)
        oled.text("Heure   "+str(printtime()['time']), 0, 10)
        oled.text("Temperature"+str(data['temperature'])+"C", 0, 20)
        oled.text("Humidite   "+str(data['humidite'])+"%", 0, 30)
        #oled.text("Network       "+str(api_ping), 0, 40, 1)
        #oled.text("Wifi    "+str(wifi), 0, 40, 1)
        oled.text("API          "+str(status_api), 0, 40)
        oled.text("IP "+str(IP), 0, 50)
        oled.show()
        data_to_send = {'temperature': str(data['temperature']),'humidite': str(data['humidite']), 'api_key': str(api_key)}
        print(data_to_send)
        headers = {}
        headers['Content-Type'] = 'application/json'
        #micropython.alloc_emergency_exception_buf(100)
        try:
            r = requests.post(api_url, json=data_to_send, headers=headers)
        except:
            print("POST HTTP Weather Data over API Failed")
            print(api_url)
            print(data_to_send)
            print(headers)
        try:
            del(data_to_send)
            del(r)
            del(headers)
            del(status_api)
        except:
            pass
        print("main loop end and done at "+str(printtime()['time'])+", we will wait "+str(wait_time)+" secondes")
        time.sleep(wait_time)
        try:
            ntptime.settime()
        except:
            pass
        #del(ping_api)
        gc.collect()
        gc.threshold(gc.mem_free() // 4 + gc.mem_alloc())
        
        
if __name__ == "__main__":
    print("manual run mode")
    runtheloop()
