# ESP8266-01


## Flash avec ESPTool.py

```bash
# Detecter l'ESP avec esptool et trouver la taille de sa mémoire flash
esptool.py --port /dev/ttyUSB0 flash_id
# Effacer la mémoire flash
esptool.py --port /dev/ttyUSB0 erase_flash
# Ecrire un firmware sur la mémoire flash
esptool.py --port /dev/ttyUSB0 write_flash 0x0 firmware_micropython_esp8266-20210202-v1.14.bin


esptool.py --port /dev/ttyUSB0 erase_flash && esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --verify --flash_size=detect -fm dio 0 firmware_micropython_esp8266-1m-20200902-v1.13.bin

rshell -d
CONNECT serial /dev/ttyUSB0
```
