import network
import ujson
import time

ap_if = network.WLAN(network.AP_IF)
# Disable the built-in AP
if ap_if.active():
    ap_if.active(False)

def connect():
    # Station = activation mode client wifi
    station = network.WLAN(network.STA_IF)
    
    # Lecture du fichier de config
    with open('config.json') as config_file:
        config = ujson.load(config_file)
        ssid = config['wifi']['ssid']
        password = config['wifi']['password']
        mode = config['ip']['mode']
    print("Network config mode read in config file : "+str(mode))
    
    # Si deja connecte
    if station.isconnected() == True:
        print("ESP is already connected")
    # Sinon tant que non connecte on ressaie toutes les 10 secondes
    else:
        station.active(True)
        station.connect(ssid, password)
        time.sleep(3)
        while station.isconnected() == False:
            station = network.WLAN(network.STA_IF)
            print("Station connecté "+str(station.isconnected()))
            print("ESP failed to connect, please check "+str(ssid)+" with key "+str(password)+" is available, we retry to connect in 10 seconds")      
            time.sleep(10)
            station.active(True)
            station.connect(ssid, password)
        # Selon mode DHCP ou static on applique les paramètres réseaux
        if mode == "static":
            print("We connect to network with static ip mode")
            ip = config['ip']['ip']
            subnet = config['ip']['subnet']
            gateway = config['ip']['gateway']
            dns = config['ip']['dns']
            #print("We define these settings from config file :"+str(ip, subnet, gateway, dns))
            station.ifconfig((ip,subnet,gateway,dns))
        else:
            print("We connect to network with DHCP mode")
    # On affiche en console les paramètres réseaux
    print("Actual network settings in apply :\n IP : "+str(station.ifconfig()[0])+"\n Subnet Mask : "+str(station.ifconfig()[1])+"\n Gateway : "+str(station.ifconfig()[2])+"\n DNS : "+str(station.ifconfig()[3]))
    print("Connection successful")

def disconnect():
    print("disconnect")
    import network
    station = network.WLAN(network.STA_IF)
    try:
        station.disconnect()
        station.active(False)
    except:
        print("Disconnect failed check if always connected")

#disconnect()
#time.sleep(15)
connect()
#import webrepl
#webrepl.start()