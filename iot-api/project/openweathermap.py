import requests
import json

# Read config file
from configparser import ConfigParser
config = ConfigParser()
config.read("config.ini")
api_key = config['External']['openweathermap_api']


def query_api(lat, long):
    url = "https://api.openweathermap.org/data/2.5/onecall?lat=%s&lon=%s&appid=%s&units=metric&lang=fr" % (lat, long, api_key)
    response = requests.get(url)
    data = json.loads(response.text)
    return data
lat = "48.208176"
long = "16.373819"
print(json.dumps(query_api(lat, long), indent=4))
