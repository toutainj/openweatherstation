from flask import Flask
from flask_restx import Resource, Api, fields, reqparse
import pymysql
from werkzeug.routing import FloatConverter as BaseFloatConverter # convert float into negative float if need that flask can handle correctly in route
from secrets import token_urlsafe
import datetime, json, time
from geopy.geocoders import Nominatim # for GPS to address
# Usefull ressources :
## https://flask-restplus.readthedocs.io/en/stable/index.html
## https://blog.invivoo.com/designer-des-apis-rest-avec-flask-restplus/
## https://fr.wikipedia.org/wiki/Liste_des_codes_HTTP

### Weather Forecast Api
import openweathermap

# Config Flask App Definition
app = Flask(__name__)
api = Api(app=app, version="0.1", doc="/api", title="Ma station météo", description="Cette API sert à interfacer base de donnée, front, et sondes météo", default="API", default_label='API', validate=True)
# Config Flask Namespace Definition
namespace_sonde = api.namespace(name='sonde', description='Fonctions API pour la gestion des sondes', path='/')
namespace_sondes = api.namespace(name='sondes', description='Fonctions API pour l\'affichage des sondes', path='/')
namespace_user = api.namespace(name='user', description='Fonctions API pour la gestion des users', path='/')
namespace_users = api.namespace(name='users', description='Fonctions API pour l\'affichage des users', path='/')
namespace_data = api.namespace(name='data', description='Fonctions API pour la gestion des données', path='/')

# Read config file
from configparser import ConfigParser
config = ConfigParser()
config.read("config.ini")

# Enable/Disable API_Key check
auth_enable = config['Authentification']['enable']

# Generate db from http://filldb.info/
db_host = config['Database']['host']
db_name = config['Database']['name']
db_user = config['Database']['user']
db_password = config['Database']['password']
# db_host = "localhost"
conv=pymysql.converters.conversions.copy()
conv[246]=float    # convert decimals to floats
conv[10]=str       # convert dates to strings
try:
    db=pymysql.connect(
        host=db_host,
        user=db_user,
        passwd=db_password,
        db=db_name,
        conv=conv)
except (pymysql.err.InternalError, pymysql.err.OperationalError) as e:
    print("La DB n'existe pas elle va etre cree")
    print(repr(e))
    db=pymysql.connect(
        host=db_host,
        user=db_user,
        passwd=db_password,
        db="mysql")
    sql = ["CREATE DATABASE "+str(db_name)+";","use "+str(db_name)+";"]
    with db.cursor() as cursor:
        for query in sql:
            cursor.execute(query)

# Function for read sql file
def get_sql_from_file(filename):
    """
    Get the SQL instruction from a file
    :return: a list of each SQL query whithout the trailing ";"
    """
    from os import path
    # File did not exists
    if path.isfile(filename) is False:
        print("File load error : {}".format(filename))
        return False
    else:
        with open(filename, "r") as sql_file:
            # Split file in list
            ret = sql_file.read().split(';')
            # drop last empty entry
            ret.pop()
            return ret

# Check if DB has content else create schema or sample data
try:
    with db.cursor() as cursor:
        sql = "SELECT `id` FROM `sonde` WHERE `id`=%s"
        cursor.execute(sql, (1))
        result = cursor.fetchone()
        print("Il existe au moins une sonde dans la DB")
        if result==None:
            print("Les tables de la DB sont vides on importe un peu de contenu")
            #import_sql_filename = "./db_schema.sql"
            import_sql_filename = "./db_with_sampledata.sql"
            request_list = get_sql_from_file(import_sql_filename)
            if request_list is not False:
                for idx, sql_request in enumerate(request_list):
                    with db.cursor() as cursor:
                        print(sql_request)
                        cursor.execute(sql_request + ';')
                        db.commit()
except pymysql.err.ProgrammingError:
    print("La DB est vide")
    import_sql_filename = "./db_schema.sql"
    request_list = get_sql_from_file(import_sql_filename)
    if request_list is not False:
        for idx, sql_request in enumerate(request_list):
            with db.cursor() as cursor:
                print(sql_request + ';')
                cursor.execute(sql_request + ';')

# Définition modèles des données
sondes_resource_fields = api.model('SondeResource', {
    # 'id': fields.String(description='ID de la sonde', default='1', example='1', required=False),
    'name': fields.String(description='Nom de la sonde', default='MobileProto', example='Ma Sonde', required=False),
    'status': fields.String(description='Statut de la sonde', default='1', example='1', required=False),
    'lat': fields.String(description='Latitude de la sonde', default='48.7026292', example='48.7026292', required=False),
    'long': fields.String(description='Longitude de la sonde', default='2.4773815', example='2.4773815', required=False),
    # 'added': fields.String(description='Date d\'ajout de la sonde', default='2021-01-27 20:00:00', example='2021-01-27 20:00:00', required=False),
    # 'updated': fields.String(description='Date de mise à jour de la sonde', default='2021-01-27 20:00:00', example='2021-01-27 20:00:00', required=False),
    # 'email': fields.String(description='Email du propriétaire de la sonde', default='toutainj@gmail.com', example='toutainj@gmail.com', required=False),
    'api_key': fields.String(description='Clé API de la sonde', default='azerty123456789', example='azerty123456789', required=False),
})
sondes_arguments = reqparse.RequestParser()
sondes_arguments.add_argument('id')
sondes_arguments.add_argument('name')
sondes_arguments.add_argument('status')
sondes_arguments.add_argument('lat')
sondes_arguments.add_argument('long')
sondes_arguments.add_argument('added')
sondes_arguments.add_argument('updated')
sondes_arguments.add_argument('api_key')
sondes_arguments.add_argument('user_id')

users_resource_fields = api.model('UserResource', {
    # 'id': fields.String(description='ID de la sonde', default='1', example='1', required=False),
    'username': fields.String(description='Nom de l\'utilisateur', default='username', example='mon-pseudo', required=True),
    'lastname': fields.String(description='Nom de l\'utilisateur', default='name', example='mon-nom', required=True),
    'firstname': fields.String(description='Prénom de l\'utilisateur', default='firstname', example='mon-prenom', required=True),
    'status': fields.String(description='Statut du compte', default='1', example='1', required=False),
    # 'added': fields.String(description='Date d\'ajout de la sonde', default='2021-01-27 20:00:00', example='2021-01-27 20:00:00', required=False),
    # 'updated': fields.String(description='Date de mise à jour de la sonde', default='2021-01-27 20:00:00', example='2021-01-27 20:00:00', required=False),
    'email': fields.String(description='Email du compte', default='contactme@gmail.com', example='contactme@gmail.com', required=True),
    'password': fields.String(description='Password du compte', default='', example='', required=True),
    'user_api_key': fields.String(description='Clé API du compte', default='azerty123456789', example='azerty123456789', required=False),
})
users_arguments = reqparse.RequestParser()
users_arguments.add_argument('id')
users_arguments.add_argument('username')
users_arguments.add_argument('lastname')
users_arguments.add_argument('firstname')
users_arguments.add_argument('status')
users_arguments.add_argument('added')
users_arguments.add_argument('updated')
users_arguments.add_argument('email')
users_arguments.add_argument('password')
users_arguments.add_argument('api_key')


data_resource_fields = api.model('DataResource', {
    # 'id': fields.String(description='ID de la donnée', default='1', example='1', required=False),
    'sonde_id': fields.String(description='ID de la sonde', default='1', example='1', required=False),
    'temperature': fields.String(description='Température relevé par la sonde (en °C)', default='10', example='20', required=False),
    'humidite': fields.String(description='Humidité relevé par la sonde (en %)', default='50', example='55', required=False),
    # 'datetime': fields.String(description='Timestamp de la donnée', default='1', example='1', required=False),
    'api_key': fields.String(description='Clé API de la sonde', default='azerty123456789', example='azerty123456789', required=False),
})
data_arguments = reqparse.RequestParser()
data_arguments.add_argument('sonde_id', type=int)
data_arguments.add_argument('data_mode', type=str, help='data_mode : api/all/last', location='args')
data_arguments.add_argument('filter_data', type=str, help='filter_data : temperature/humidite', location='args')
# data_arguments.add_argument('temperature')
# data_arguments.add_argument('humidite')
# data_arguments.add_argument('datetime')
# data_arguments.add_argument('api_key')

# Converter for negative float in flask routes
class FloatConverter(BaseFloatConverter):
    regex = r'-?\d+(\.\d+)?'
app.url_map.converters['float'] = FloatConverter

### DEMO with simple api function via HTTP GET in default namespace
@api.route("/api/v1/ping")
class Ping(Resource):
    @api.response(200, 'API Ping : Success')
    @api.response(400, 'API Ping: Error')
    @api.response(403, 'API Ping: Ceci n\'est pas autorisé')
    def get(self):
        """
        Test de l'API avec un simple ping
        """
        return {'response':'pong'}, 200

@api.route("/api/v1/time")
class Time(Resource):
    @api.response(200, 'Flask Time : Success')
    @api.response(400, 'Flask DateTime: Error')
    def get(self):
        """
        Renvoi la date actuelle et le timestamp
        """
        current_timestamp = datetime.datetime.now().timestamp()
        current_date = datetime.datetime.now()
        return {'response':{
                    'current_date':str(current_date),
                    'current_timestamp':str(current_timestamp)}},200

@namespace_sonde.route("/api/v1/sonde/<int:id>")
class Sonde(Resource):
    @api.doc(params={'id': 'ID de la sonde à interroger'})
    def get(self, id):
        """
        Retourner une sonde pour un ID donné
        """
        sql = "SELECT id, name, type, status, is_public, lat, `long`, city, code_city, country, code_country,\
        date_format(added,'%d-%m-%y %H:%i:%S') as added,  date_format(updated,'%d-%m-%y %H:%i:%S') as updated, api_key, user_id, location_raw \
        FROM sonde WHERE id="+str(id)+";"
        print(sql)
        with db.cursor(pymysql.cursors.DictCursor) as cursor:
            cursor.execute(sql)
            result = cursor.fetchone()
            result['location_raw'] = str(result['location_raw'])
            cursor.close()
        return result
    @api.doc(params={'id': 'Id de la sonde à supprimer'})
    @api.response(200, 'API User : Deleted with Success')
    @api.response(204, 'API User : Sonde not found cant be deleted')
    def delete(self, id):
        """
        Supprimer une sonde pour un ID donné
        """
        sql = "DELETE from sonde WHERE id=%s RETURNING id"
        with db.cursor(pymysql.cursors.DictCursor) as cursor:
            result = cursor.execute(sql, id)
            db.commit()
            cursor.close()
        if int(result) > 0:
            return {'sonde_deleted_with_success':str(result)}, 200
        else:
            return {'error':'no sonde found to delete'}, 204
# UPDATE sonde from id
    @api.expect(sondes_resource_fields, validate=True)
    @api.doc(params={'id': 'ID de la sonde à mettre à jour'})
    @api.response(200, 'API Sonde : Updated with Success')
    @api.response(204, 'API Sonde : Integrity error (check if email or api key already exist ?)')
    def put(self, id):
        """
        Mettre à jour les données d'une sonde
        """
        # sonde_api_key = token_urlsafe(32)
        sql = "UPDATE sonde SET `name` = %s, `status` = %s, `lat` = %s, `long` = %s, `email` = %s, `api_key` = %s WHERE `id`="+str(id)+";"
        print(api.payload)
        print(sql)
        with db.cursor() as cursor:
            try:
                cursor.execute(sql, (str(api.payload['name']), str(api.payload['status']), str(api.payload['lat']), str(api.payload['long']), str(api.payload['email']), str(api.payload['api_key'])))
                db.commit()
                last_id = cursor.lastrowid
                cursor.close()
                return {'API sonde': 'sonde with id '+str(last_id)+' was updated'}, 200
            except pymysql.err.IntegrityError:
                return {'sonde wont be created': 'integrity error'}, 204

@namespace_sonde.route("/api/v1/sonde/<string:name>")
class Sonde(Resource):
    @api.doc(params={'nom': 'Nom de la sonde à interroger'})
    def get(self, name):
        """
        Retourner une sonde pour un nom donné
        """
        sql = "SELECT `id`, `name`, `type`, `status`, `is_public`, `lat`, `long`, `city`, `code_city`, `country`, `code_country`,\
        date_format(`added`,'%d-%m-%y %H:%i:%S') as added,  date_format(`updated`,'%d-%m-%y %H:%i:%S') as updated, `api_key`, `user_id`, ̀`location_raw`\
        FROM `sonde` WHERE `name` LIKE '%"+str(name)+"%';"
        with db.cursor(pymysql.cursors.DictCursor) as cursor:
            cursor.execute(sql)
            result = cursor.fetchone()
            cursor.close()
        return result

# POST new sonde without specifying an ID in route (use autoincrement from mariadb)
@namespace_sonde.route("/api/v1/sonde")
class Sonde(Resource):
    @api.expect(sondes_resource_fields, validate=True)
    @api.response(200, 'API Sonde : Created with Success')
    @api.response(204, 'API Sonde : Integrity error (check if email or api key already exist ?)')
    def post(self):
        """
        Créer une sonde
        """
        sonde_api_key = token_urlsafe(32)
        sql = "INSERT INTO sonde (`name`, `status`, `lat`, `long`, `email`, `api_key`) VALUES (%s, %s, %s, %s, %s, %s);"
        with db.cursor() as cursor:
            try:
                cursor.execute(sql, (str(api.payload['name']), str(api.payload['status']), str(api.payload['lat']), str(api.payload['long']), str(api.payload['email']), str(sonde_api_key)))
                db.commit()
                last_id = cursor.lastrowid
                cursor.close()
                return {'sonde created with id':str(last_id)}, 200
            except pymysql.err.IntegrityError:
                return {'sonde wont be created': 'integrity error'}, 204


@namespace_sondes.route("/api/v1/sondes")
class Sondes(Resource):
    @api.response(200, 'API Sondes : Got list with success')
    @api.response(204, 'API Sondes : Got an empty list')
    def get(self):
        sql = "SELECT id, name, type, status, is_public, lat, `long`, city, code_city, country, code_country,\
        date_format(added,'%d-%m-%y %H:%i:%S') as added,  date_format(updated,'%d-%m-%y %H:%i:%S') as updated, api_key, user_id, location_raw \
        FROM sonde;"
        print(sql)
        with db.cursor(pymysql.cursors.DictCursor) as cursor:
            cursor.execute(sql)
            results = cursor.fetchall()
            cursor.close()
            for result in results:
                result['location_raw'] = str(result['location_raw'])
            [[str(val) for val in row] for row in results]
            if results:
                return results, 200
            else:
                results = None
                return {'error': 'no sonde to display'}, 204

@namespace_user.route("/api/v1/user/<string:username>")
class User(Resource):
    @api.doc(params={'username': 'username de l\'utilisateur à interroger'})
    def get(self, username):
        """
        Retourner un utilisateur pour un username donné
        """
        sql = "SELECT `id`, `username`, `lastname`, `firstname` `status`, date_format(`added`,'%d-%m-%y %H:%i:%S') as added,  date_format(`updated`,'%d-%m-%y %H:%i:%S') as updated, `email`, `password`, `user_api_key` FROM `user` WHERE `username`='"+str(username)+"';"
        with db.cursor(pymysql.cursors.DictCursor) as cursor:
            cursor.execute(sql)
            result = cursor.fetchone()
            print(result)
            cursor.close()
        return result

@namespace_user.route("/api/v1/user/<int:id>")
class User(Resource):
    @api.doc(params={'id': 'ID de l\'utilisateur à interroger'})
    def get(self, id):
        """
        Retourner un utilisateur pour un ID donné
        """
        sql = "SELECT `id`, `username`, `lastname`, `firstname` `status`, date_format(`added`,'%d-%m-%y %H:%i:%S') as added,  date_format(`updated`,'%d-%m-%y %H:%i:%S') as updated, `email`, `password`, `user_api_key` FROM `user` WHERE `id`="+str(id)+";"
        with db.cursor(pymysql.cursors.DictCursor) as cursor:
            cursor.execute(sql)
            result = cursor.fetchone()
            cursor.close()
        return result
    @api.doc(params={'id': 'Id de l\'utilisateur à supprimer'})
    @api.response(200, 'API User : Deleted with Success')
    @api.response(204, 'API User : Sonde not found cant be deleted')
    def delete(self, id):
        """
        Supprimer un utilisateur pour un ID donné
        """
        sql = "DELETE from user WHERE id=%s RETURNING id"
        with db.cursor(pymysql.cursors.DictCursor) as cursor:
            result = cursor.execute(sql, id)
            db.commit()
            cursor.close()
        if int(result) > 0:
            return {'user_deleted_with_success':str(result)}, 200
        else:
            return {'error':'no sonde found to delete'}, 204
# UPDATE user from id
    @api.expect(users_resource_fields, validate=True)
    @api.doc(params={'id': 'ID de l\'utilisateur à mettre à jour'})
    @api.response(200, 'API User : Updated with Success')
    @api.response(204, 'API User : Integrity error (check if email or api key already exist ?)')
    def put(self, id):
        """
        Mettre à jour les données d\'un utilisateur
        """
        # sonde_api_key = token_urlsafe(32)
        sql = "UPDATE user SET `lastname` = %s, `firstname` = %s, `status` = %s, `email` = %s, `password` = %s, `user_api_key` = %s WHERE `id`="+str(id)+";"
        print(api.payload)
        print(sql)
        with db.cursor() as cursor:
            try:
                cursor.execute(sql, (str(api.payload['last']), str(api.payload['firstname']), str(api.payload['status']), str(api.payload['email']), str(api.payload['password']), str(api.payload['user_api_key'])))
                db.commit()
                last_id = cursor.lastrowid
                cursor.close()
                return {'API User': 'User with id '+str(last_id)+' was updated'}, 200
            except pymysql.err.IntegrityError:
                return {'User wont be updated': 'integrity error'}, 204

# @namespace_user.route("/api/v1/user/<string:name>")
# class Sonde(Resource):
#     @api.doc(params={'email': 'Email du compte à interroger'})
#     @api.doc(params={'username': 'Username du compte à interroger'})
#     @api.doc(params={'password': 'Password du compte à interroger'})
#     def get(self, name):
#         """
#         Retourner un utilisateur pour un nom d\'utilisateur ou email donné
#         """
#         sql = "SELECT `id`, `username`, `lastname`, `firstname`, `status`, `lat`, `long`, date_format(`added`,'%d-%m-%y %H:%i:%S') as added,  date_format(`updated`,'%d-%m-%y %H:%i:%S') as updated, `email`, `user_api_key` FROM `user` WHERE `password`="+str(password)+" AND `username` LIKE '%"+str(name)+"%' or `email` LIKE '%"+str(name)+"%';"
#         with db.cursor(pymysql.cursors.DictCursor) as cursor:
#             cursor.execute(sql)
#             result = cursor.fetchone()
#             cursor.close()
#         return result

# POST new sonde without specifying an ID in route (use autoincrement from mariadb)
@namespace_user.route("/api/v1/user")
class User(Resource):
    @api.expect(users_resource_fields, validate=True)
    @api.response(200, 'API User : Created with Success')
    @api.response(204, 'API User : Integrity error (check if email or api key already exist ?)')
    def post(self):
        """
        Créer un utilisateur
        """
        print(api.payload)
        user_api_key = token_urlsafe(32)
        sql = "INSERT INTO user (`username`, `lastname`, `firstname`, `status`, `email`, `password`, `user_api_key`) VALUES (%s, %s, %s, %s, %s, %s, %s);"
        with db.cursor() as cursor:
            try:
                cursor.execute(sql, (str(api.payload['username']), str(api.payload['lastname']), str(api.payload['firstname']), str(api.payload['status']), str(api.payload['email']), str(api.payload['password']), str(user_api_key)))
                db.commit()
                last_id = cursor.lastrowid
                cursor.close()
                return {'user created with id':str(last_id)}, 200
            except pymysql.err.IntegrityError:
                return {'user wont be created': 'integrity error'}, 204


@namespace_users.route("/api/v1/users")
class Users(Resource):
    @api.response(200, 'API Users : Got list with success')
    @api.response(204, 'API Users : Got an empty list')
    def get(self):
        sql = "SELECT `id`, `username`, `lastname`, `firstname`, `status`, `email`, `password`, date_format(`added`,'%d-%m-%y %H:%i:%S') as added,  date_format(`updated`,'%d-%m-%y %H:%i:%S') as updated, `user_api_key` FROM `user`;"
        with db.cursor(pymysql.cursors.DictCursor) as cursor:
            cursor.execute(sql)
            results = cursor.fetchall()
            cursor.close()
            [[str(val) for val in row] for row in results]
            if results:
                return results, 200
            else:
                results = None
                return {'error': 'no users to display'}, 204

# @namespace_data.route("/api/v1/data/<int:sonde_id>/<data_mode>")
# @namespace_data.route("/api/v1/data/<int:sonde_id>/<string:data_mode>")
@namespace_data.route("/api/v1/data/")
# @namespace_data.route("/api/v1/data/<int:sonde_id>")
@namespace_data.route("/api/v1/data/", defaults={'sonde_id': None, 'data_mode': 'last', 'filter_data': 'temperature'})

class Data(Resource):
    @api.expect(data_arguments)
    @api.doc(params={'sonde_id': 'ID de la sonde à interroger'})
    @api.doc(params={'data_mode': 'Quantité de donnée à récuperer (all/last/api/group_per_day)'})
    @api.doc(params={'filter_data': 'Filtrer les données (temperature/humidite)'})
    def get(self, sonde_id, data_mode, filter_data):
        data = data_arguments.parse_args()
        if data.get('data_mode') == "all" or data.get('data_mode') == "api":
            sql = "SELECT `id`, `sonde_id`, `temperature`, `humidite`, date_format(`datetime`,'%d-%m-%y %H:%i:%S') as datetime FROM `data` \
                WHERE `sonde_id`='"+str(data.get('sonde_id'))+"';"
        elif data.get('data_mode') == "group_per_day":
            sql = "SELECT count(`id`) as nb_measures, DATE(datetime) AS jour, MIN(temperature) as temp_mini, MAX(temperature) as temp_max, AVG(temperature) AS temperature, AVG(humidite) AS humidite FROM `data`\
              WHERE `sonde_id`='"+str(data.get('sonde_id'))+"' GROUP BY DATE(datetime) ORDER BY jour;"
        else: # data.get('data_mode') == "last":
            sql = "SELECT `id`, `sonde_id`, `temperature`, `humidite`, date_format(`datetime`,'%d-%m-%y %H:%i:%S') as datetime FROM `data` \
                WHERE `sonde_id`='"+str(data.get('sonde_id'))+"' ORDER BY id DESC limit 1;"

        if data.get('data_mode') == "api":
            with db.cursor(pymysql.cursors.DictCursor) as cursor:
                try:
                    cursor.execute(sql)
                    results_full = cursor.fetchall()
                    cursor.close()
                    data_temperature = []
                    data_humidite = []
                    data_datetime = []
                    for result in results_full:
                        data_temperature.append([int(datetime.datetime.strptime(result['datetime'], "%d-%m-%y %H:%M:%S").timestamp()*1000+3600000), float(result['temperature'])])
                        data_humidite.append([int(datetime.datetime.strptime(result['datetime'], "%d-%m-%y %H:%M:%S").timestamp()*1000+3600000), float(result['humidite'])])
                    results = [data_temperature, data_humidite] # work with graph.html
                    #     data_temperature.append(float(result['temperature']))
                    #     data_humidite.append(float(result['humidite']))
                    #     data_datetime.append(int(datetime.datetime.strptime(result['datetime'], "%d-%m-%y %H:%M:%S").timestamp()*1000))
                    # results = [{'name':'datetime','data':[data_datetime]},{'name':'temperature','data':[data_temperature]},{'name':'humidite','data':[data_humidite]}]
                except Exception as e:
                    results = "Pas OU trop de résultat \nError:"+str(e)
        else:
            with db.cursor(pymysql.cursors.DictCursor) as cursor:
                try:
                    cursor.execute(sql)
                    results = cursor.fetchall()
                    [[str(val) for val in row] for row in results]
                    # print(results)
                except Exception as e:
                    results = "Pas OU trop de résultat \nError:"+str(e)
        print(results)
        return results

# POST new data without specifying an ID (use autoincrement from mariadb) REQUIRE API_KEY !!
@namespace_data.route("/api/v1/data")
class Data(Resource):
    @api.expect(data_resource_fields, validate=True)
    @api.response(200, 'API Data : Created with Success')
    @api.response(204, 'API Data : Integrity error (check if api key exist ?)')
    def post(self):
        sql_sonde_id = "SELECT `id` from sonde WHERE api_key=%s ;"
        sql_sondekey = "SELECT api_key from sonde WHERE id=%s ;"
        sql = "INSERT INTO data (sonde_id, temperature, humidite) VALUES (%s, %s, %s);"
        with db.cursor() as cursor:
            cursor.execute(sql_sonde_id, (api.payload['api_key']))
            try:
                sonde_id = cursor.fetchone()[0]
            except TypeError:
                return {'data error': 'cant insert data, please check API_KEY'}, 204
            if auth_enable == True:
                cursor.execute(sql_sondekey, sonde_id)

                sonde_key = cursor.fetchone()[0]

                if str(sonde_key) == str(api.payload['api_key']):
                    cursor.execute(sql, (sonde_id, api.payload['temperature'], api.payload['humidite']))
                    db.commit()
                    last_id = cursor.lastrowid
                    cursor.close()
                    return last_id
                else:
                    # return {'Error': 'Check your API Key'}
                    print("authentification per api_key disabled")
                    pass
            else:
                cursor.execute(sql, (sonde_id, api.payload['temperature'], api.payload['humidite']))
                db.commit()
                last_id = cursor.lastrowid
                cursor.close()
                return { "data": "data inserted for sonde_id="+str(sonde_id)+" with data id="+str(last_id)}, 200

@api.route("/api/v1/gps/<int:sonde_id>")
# @api.route("/api/v1/gps/<float:lat>/<float:long>")
class GPS(Resource):
    # @api.doc(params={'lat': 'Latitude de l\'adresse'})
    # @api.doc(params={'long': 'Longitude de l\'adresse'})
    @api.doc(params={'sonde_id': 'ID de la sonde à géolocaliser'})
    # def get(self, lat, long):
    def get(self, sonde_id):
        """
        Retourner une adresse pour une latitude/longitude donnée
        """
        sql = "SELECT lat, `long`, location_raw, city, code_city, country, code_country FROM sonde WHERE id="+str(sonde_id)
        with db.cursor(pymysql.cursors.DictCursor) as cursor:
            # try:
            cursor.execute(sql)
            result = cursor.fetchone()
            # print("### DEBUG ###")
            # print(result)
            # print("### DEBUG ###")
            if result['location_raw'] == None:
                geolocator = Nominatim(user_agent="OpenWeatherStation")
                location = geolocator.reverse(str(result['lat'])+", "+str(result['long']))
                try:
                    city = location.raw['address']['city']
                except:
                    try:
                        city = location.raw['address']['town']
                    except:
                        try:
                            city = location.raw['address']['municipality']
                        except:
                            city = location.raw['display_name']
                try:
                    postal_code = location.raw['address']['postcode']
                except:
                    postal_code = ""
                pays = location.raw['address']['country']
                code_pays = location.raw['address']['country_code']
                location_raw = location.raw
                # print(location.address)
                # print(location.raw)
                # print(city)
                # print(postal_code)
                update_sql = "UPDATE sonde SET location_raw=\'"+str(json.dumps(location.raw).replace("\'", "\""))+"\', city='"+str(city)+"', code_city='"+str(postal_code)+"', country='"+str(pays)+"', code_country='"+str(code_pays)+"' WHERE id="+str(sonde_id)+";"
                # print("### DEBUG ###")
                # print(update_sql)
                # print("### DEBUG ###")
                cursor.execute(update_sql)
                db.commit()
                last_id = cursor.lastrowid
                print("Sonde "+str(last_id)+ " a bien été mise à jour avec ses infos de geoloc")
            else:
                city = result['city']
                postal_code = result['code_city']
                pays = result['country']
                code_pays = result['code_country']
                location_raw = str(result['location_raw'])
            cursor.close()
        return { "city": city, "code_postal": postal_code, "pays": pays, "code_pays": code_pays, "raw": location_raw}

@api.route("/api/v1/weatherforecast/<int:sonde_id>")
# @api.route("/api/v1/weatherforecast/<int:sonde_id>/<float:lat>/<float:long>")
class WeatherForecast(Resource):
    def get(self, sonde_id):
    # def get(self, sonde_id, lat, long):
        with db.cursor(pymysql.cursors.DictCursor) as cursor:
            sql_last_update = "SELECT updated from forecast_data WHERE sonde_id=%s ;"
            cursor.execute(sql_last_update, sonde_id)
            last_update = cursor.fetchone()
            print(last_update)
            now = datetime.datetime.now()
            # delta = now - last_update['updated']

            # print(delta)
            # print(type(delta))
            if last_update == None:
                forecast_no_exist = True
                delta = datetime.timedelta(hours=24)
            else:
                forecast_no_exist = False
                delta = now - last_update['updated']
            print("DELTA="+str(delta))
            print(forecast_no_exist)
            if delta > datetime.timedelta(hours=12) or forecast_no_exist == True:
                print("#DEBUG# We query openweathermap API")
                sql_geoloc_sonde = "SELECT lat, `long` from sonde WHERE id=%s"
                cursor.execute(sql_geoloc_sonde, sonde_id)
                sonde_geoloc = cursor.fetchone()
                lat = sonde_geoloc['lat']
                long = sonde_geoloc['long']
                print(sonde_geoloc)
                forecast = openweathermap.query_api(lat,long)
        #         # print(json.dumps(forecast))
        #         # print(type(json.dumps(forecast)))
        #         # forecast_data = forecast.query_(city)
                sql = "INSERT INTO forecast_data (sonde_id, json) VALUES ("+str(sonde_id)+", \'"+str(json.dumps(forecast).replace("\'", "\""))+"\');"
                update_sql = "UPDATE forecast_data SET json=\'"+str(json.dumps(forecast, ensure_ascii=False).replace("\'", "\""))+"\' WHERE sonde_id="+str(sonde_id)+";"
                try:
                    print(sql)
                    cursor.execute(sql)
                except (pymysql.err.IntegrityError, pymysql.err.InternalError) as e:
                    print("error:"+repr(e))
                    print(update_sql)
                    cursor.execute(update_sql)
                db.commit()
                last_id = cursor.lastrowid
            else:
                print("No update needed we return sql data")
                # sql_data = "SELECT json,  date_format(`updated`,'%d-%m-%y %H:%i:%S') as updated FROM forecast_data WHERE sonde_id="+str(sonde_id)+";"
                sql_data = "SELECT json FROM forecast_data WHERE sonde_id="+str(sonde_id)+";"
                cursor.execute(sql_data)
                # forecast = cursor.fetchone()
                forecast = json.loads(cursor.fetchone()['json'])
                #print(forecast)
                # forecast = forecast.replace("\'", "\"")
                # print(forecast)
        cursor.close()
        del(delta, last_update)
        return forecast



if __name__ == '__main__':
    app.run(host=str(config['General']['listen_ip']), port=int(config['General']['listen_port']), debug=config['General']['debug'])
