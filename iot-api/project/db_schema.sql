#
# TABLE STRUCTURE FOR: authors
#

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` boolean NOT NULL default 0,
  `is_admin` boolean NOT NULL default 0,
  `added` datetime NOT NULL DEFAULT current_timestamp(),
  `updated` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `email` varchar(63) NOT NULL,
  `password` varchar(63) NOT NULL,
  `user_api_key` varchar(63) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `user_api_key` (`user_api_key`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (`id`, `username`, `lastname`, `firstname`, `status`, `is_admin`, `email`, `password`, `user_api_key`) VALUES (1, 'xila', 'Toutain', 'Julien', '1', '1', 'toutainj@gmail.com', 'azerty', 'azerty123456789');

DROP TABLE IF EXISTS `sonde`;

CREATE TABLE `sonde` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` boolean NOT NULL default 0,
  `is_public` boolean NOT NULL default 0,
  `lat` numeric(11,8) NOT NULL,
  `long` numeric(11,8) NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci,
  `code_city` varchar(50) COLLATE utf8_unicode_ci,
  `country` varchar(50) COLLATE utf8_unicode_ci,
  `code_country` varchar(50) COLLATE utf8_unicode_ci,
  `added` datetime NOT NULL DEFAULT current_timestamp(),
  `updated` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `api_key` varchar(63) NOT NULL,
  `user_id` SMALLINT UNSIGNED NOT NULL,
  `location_raw` blob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_key` (`api_key`),
  CONSTRAINT `fk_user_id_sonde` FOREIGN KEY (`user_id`) REFERENCES user(`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `sonde` (`id`, `name`, `type`, `status`, `is_public`, `lat`, `long`, `api_key`, `user_id`) VALUES (1, 'MobileProto', 'indoor', '1', '1', '48.70281356624354', '2.4818941905526297', 'azerty123456789', 1);
INSERT INTO `sonde` (`id`, `name`, `type`, `status`, `is_public`, `lat`, `long`, `api_key`, `user_id`) VALUES (2, 'MiniMobileProto', 'indoor', '1', '1', '49.46842575073242', '1.075634479522705', 'azerty1234567890', 1);

DROP TABLE IF EXISTS `data`;

CREATE TABLE `data` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `sonde_id` SMALLINT UNSIGNED NOT NULL,
  `temperature` numeric(3,1) COLLATE utf8_unicode_ci NOT NULL,
  `humidite` numeric(3,1) COLLATE utf8_unicode_ci NOT NULL,
  `datetime` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
INSERT INTO `data` VALUES (1,1,25.7,39.0,'2021-01-28 12:16:20'),(2,1,25.6,38.8,'2021-01-28 12:16:36'),(3,2,25.7,39.0,'2021-01-28 12:16:20'),(4,2,25.6,38.8,'2021-01-28 12:16:36');

DROP TABLE IF EXISTS `forecast_data`;

CREATE TABLE `forecast_data` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `sonde_id` SMALLINT UNSIGNED NOT NULL,
  `provider` varchar(50) NOT NULl DEFAULT 'openweathermap',
  `api_key` varchar(50),
  `json` blob NOT NULL,
  `updated` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_key` (`api_key`),
  UNIQUE KEY `sonde_id` (`sonde_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
INSERT INTO `forecast_data` (`id`, `sonde_id`, `provider`, `api_key`, `json`) VALUES (1, 1, "openweathermap", "", "");
