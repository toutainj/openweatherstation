import requests
import json

# Read config file
from configparser import ConfigParser
config = ConfigParser()
config.read("config.ini")
api_baseurl = config['API']['proto'] + "://" +\
 config['API']['host'] + ":" + str(config['API']['port']) +\
 config['API']['base_url'] + config['API']['version']


def get_sondes():
    r = requests.get(api_baseurl+"/sondes")
    return r.json()


def get_sonde(id):
    r = requests.get(api_baseurl+"/sonde/"+str(id))
    return r.text


def get_sonde_from_name(name):
    r = requests.get(api_baseurl+"/sonde/"+str(name))
    return r.text


def get_data(sonde_id, data_mode="last"):
    r = requests.get(api_baseurl+"/data/?sonde_id="+str(sonde_id)+"&data_mode="
                     + str(data_mode))
    return r.text


def get_city_from_coordonates(sonde_id):
    r = requests.get(api_baseurl+"/gps/"+str(sonde_id))
    return r.json()


def get_forecast_data(sonde_id):
    r = requests.get(api_baseurl+"/weatherforecast/"+str(sonde_id))
    return r.json()


#  Admin Functions #

def change_sonde_status(name, status, lat, long, email, api_key, sonde_id):
    headers = dict()
    headers["Content-Type"] = "application/json"
    data = {'name': name,
            'status': status,
            'lat': lat, 'long': long,
            'email': email,
            'api_key': api_key}
    r = requests.put(api_baseurl+"/sonde/"+str(sonde_id),
                     data=json.dumps(data), headers=headers)
    return r.json()


def create_sonde(name, lat, long, email):
    headers = dict()
    headers["Content-Type"] = "application/json"
    data = {'name': name,
            'lat': lat, 'long': long,
            'email': email,
            'status': "0"}
    r = requests.post(api_baseurl+"/sonde", json=data, headers=headers)
    return r.text


def create_user(username, password, firstname, lastname, email):
    headers = dict()
    headers["Content-Type"] = "application/json"
    data = {'username': username, 'password': password,
            'firstname': firstname, 'lastname': lastname, 'email': email,
            'status': '0'}
    r = requests.post(api_baseurl+"/user", json=data, headers=headers)
    return r.json()


def login_user(username):
    r = requests.get(api_baseurl+"/user/"+str(username))
    print(r.json())
    return r.json()


if __name__ == '__main__':
    print("debug")
    # print(get_sondes())
    # print(login_user('xila'))
    print(create_user('chacha', 'chacha', 'Charlotte', 'Lemonnier', 'lemonniercharlotte@gmail.com'))
    # print(get_data(sonde_id=1, data_mode='group_per_day'))
    # print(change_sonde_status(str('Ma Sonde'), str(1), str('48.7026292'), str('2.4773815'), str('toutainj@rouen.fr'), str('AlqfvLBY2khHJ8aGgqhYVyAAImmISHS6E9p-QIMCfnk'), str('3')))
    # print(get_sonde(1))
    # print(get_data(1))
