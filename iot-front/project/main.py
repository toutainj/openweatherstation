from flask import Flask, render_template, flash, redirect, url_for, jsonify, request, session
from flask_bootstrap import __version__ as FLASK_BOOTSTRAP_VERSION
from flask_bootstrap import Bootstrap
from flask_fontawesome import FontAwesome
from flask_nav.elements import Navbar, View, Subgroup, Link, Text, Separator
from markupsafe import escape
from flask_nav import Nav
import dominate
from dominate.tags import img, a
import requests, json, time
from datetime import datetime
from dateutil import parser as dateparser
import pytz
import locale
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, HiddenField
from wtforms import validators
try:
    from wtforms.fields.html5 import EmailField
except ModuleNotFoundError:
    from wtforms.fields import EmailField
from wtforms.validators import DataRequired
# from api_wrapper import get_articles, get_articles_from, get_articles_with
from api_wrapper import get_sondes, get_sonde, get_data, get_city_from_coordonates, get_forecast_data, change_sonde_status, create_sonde, login_user, create_user

# Read config file
from configparser import ConfigParser
config = ConfigParser()
config.read("config.ini")

nav = Nav()
application = Flask(__name__)
fa = FontAwesome(application)

global api_docurl
api_baseurl = config['API']['proto']+"://"+config['API']['host']+":"+str(config['API']['port'])+config['API']['base_url']+config['API']['version']
api_docurl = config['API']['base_url']

app = Flask(__name__)
app.secret_key = "fe265f8f0b5a9c07b13d340e0dcf06d1"

Bootstrap(app)
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
# branding = img(src={{ url_for('static', filename='img/logo.png') }})
def get_menu_sondes_list():
    sondes_list = get_sondes()
    # print(sondes_list)
    sondes_list_output = ['Sondes']
    for item in sondes_list:
        sonde_name = item['name']
        sonde_id = item['id']
        sondes_list_output.append(Link(sonde_name, "/graph/"+str(sonde_id)))
    return sondes_list_output

def nav_bar(login=False):
    nav.renderer()
    branding = img(_class='navbar-brand', src=config['Path']['logo'])
    if login != False:
        login_menu = Link('Se déconnecter', '/logout')
        signup_menu = Link('Mon Compte', '/myaccount')
        sondes_list = get_menu_sondes_list()
        sondes_items = Subgroup(*sondes_list)
        items = [View('Home', '.index'), View('Map', '.map'), sondes_items, Link('Admin', '/admin'), Link('API', '/doc_api'), login_menu, signup_menu]
    else:
        login_menu = Link('Se connecter', '/login')
        signup_menu = Link('S\'inscrire', '/signup')
        items = [View('Home', '.index'), View('Map', '.map'), login_menu, signup_menu]


    # tag_list = get_tags()
    # authors_items = Subgroup(*authors_list)
    # tags_items = Subgroup(*tag_list)
    # items = [View('Home', '.index'), authors_items, tags_items, Link('API', '/api')]
    # items = [View('Home', '.index'), View('Map', '.map'), Link('API', '/api')]
    # items = [View('Home', '.index'), View('Map', '.map'), sondes_items, Link('Admin', '/admin'), Link('API', '/doc_api')]
    # items = [View('Home', '.index'), View('Map', '.map'), sondes_items, Link('Admin', '/admin'), Link('API', '/doc_api'), login]
    # items.append([Text('Using Flask-Bootstrap {}'.format(FLASK_BOOTSTRAP_VERSION)), View('SignUp', '.login')])
    return Navbar(branding, *items)

nav.register_element('frontend_top', nav_bar())
nav.register_element('frontend_top_logued', nav_bar(login=True))
# nav.register_element('auth', auth_bar())
nav.init_app(app)

@app.template_filter('ctime')
def timectime(s):
    return time.ctime(s) # datetime.datetime.fromtimestamp(s)
@app.template_filter('strftime')
def _jinja2_filter_datetime(date, fmt=None):
    paris = pytz.timezone('Europe/Paris')
    date = dateparser.parse(date)
    native = date.replace(tzinfo=paris)
    format='%A %-d %B'
    try:
        locale.setlocale(locale.LC_ALL, "fr_FR.utf8")
    except:
        pass
    return native.strftime(format)
@app.context_processor
def inject_now():
    return {'now': datetime.utcnow()}

@app.route('/')
def index():
    # print(session)
    # try:
    #     if session['logged_in']:
    #         print("You are now logued !!!")
    # except KeyError:
    #     print("you are not logued")
    sondes_list = get_sondes()
    output_sondes_list = []
    for sonde in sondes_list:
        # geoloc_data = get_city_from_coordonates(sonde['lat'], sonde['long'])
        geoloc_data = get_city_from_coordonates(sonde['id'])
        forecast_data = get_forecast_data(sonde['id'])
        sonde.update({'city': geoloc_data['city'], 'postal_code': geoloc_data['code_postal'], 'country': geoloc_data['pays'], 'country_code': geoloc_data['code_pays'], 'forecast_data': forecast_data})
        # sonde.update({'forecast_data': forecast_data})
        if sonde['status'] == 1:
            print(sonde['status'])
            try:
                last_data = json.loads(get_data(sonde['id']))[0]
                sonde.update({'temperature': last_data['temperature'], 'humidite': last_data['humidite'], 'datetime': last_data['datetime']})
                last_days_data = json.loads(get_data(sonde_id=sonde['id'], data_mode='group_per_day'))
                sonde.update({'daily_data': last_days_data})
            except:
                pass
        output_sondes_list.append(sonde)
    return render_template('index.html', sondes_list=output_sondes_list)

class LoginForm(FlaskForm):
    username = StringField('Nom d\'utilisateur', validators=[DataRequired()])
    password = PasswordField('Mot de passe', validators=[DataRequired()])

class SignupForm(FlaskForm):
    username = StringField('Nom d\'utilisateur', validators=[DataRequired()])
    password = PasswordField('Mot de passe', validators=[DataRequired()])
    firstname = StringField('Prénom', validators=[DataRequired()])
    lastname = StringField('Nom', validators=[DataRequired()])
    email = EmailField('Email', validators=[DataRequired(), validators.Email()])

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        login = form.username.data
        password = form.password.data
        print(login, password)
        # login_user(login, password)
        user_data = login_user(login)
        try:
            if user_data['password'] == password:
                session['logged_in'] = True
                flash(form.username.data + ' a été identifié avec succès !')
                return redirect(url_for('index'))
        except TypeError:
            flash('L\'authentification a échouée')
            return redirect(url_for('index'))
    return render_template('login.html', form=form)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    signupform = SignupForm()
    if signupform.validate_on_submit():
        login = signupform.username.data
        password = signupform.password.data
        firstname = signupform.firstname.data
        lastname = signupform.lastname.data
        email = signupform.email.data
        user_signup = create_user(login, password, firstname, lastname, email)
        flash('Bienvenue '+ signupform.username.data + ', vérifiez votre email afin d\'activer votre compte !')
        return redirect(url_for('login'))
    return render_template('signup.html', signupform=signupform)


@app.route("/logout")
def logout():
    flash('Vous avez été déconnecté avec succès !')
    session.pop('username', None)
    session.pop('password', None)
    session.pop('logged_in', None)
    return redirect(url_for('index'))

@app.route('/map')
def map():
    sondes_list = get_sondes()
    output_sondes_list = []
    for sonde in sondes_list:
        geoloc_data = get_city_from_coordonates(sonde['id'])
        sonde.update({'city': geoloc_data['city'], 'postal_code': geoloc_data['code_postal']})
        if sonde['status'] == 1:
            last_data = json.loads(get_data(sonde['id']))[0]
            sonde.update({'temperature': float(last_data['temperature']), 'humidite': float(last_data['humidite']), 'datetime': last_data['datetime']})
            output_sondes_list.append(sonde)
    return render_template('map.html', sondes_list=output_sondes_list)

@app.route('/graph/<int:sonde_id>')
def chart(sonde_id):
    data = get_data(sonde_id, 'api')
    # return render_template('graph_v2.html', sonde_id=sonde_id, data=json.loads(data))
    return render_template('graph.html', sonde_id=sonde_id, data=json.loads(data))

@app.route('/doc_api')
def doc_api():
    iframe = api_docurl
    return render_template('index.html', iframe=iframe)

@app.route('/admin')
def admin():
    sondes_list = get_sondes()
    return render_template('admin.html', sondes=sondes_list)

@app.route('/admin',methods = ['POST'])
def admin_post():
    result = request.form.to_dict(flat=False)
    print(result)
    if result['form_action'][0] == "create_sonde":
        sonde_name = str(result['sonde_name'][0])
        sonde_lat = str(result['lat'][0])
        sonde_long = str(result['long'][0])
        sonde_email = str(result['email'][0])
        output = create_sonde(sonde_name, sonde_lat, sonde_long, sonde_email)
        status=""
    elif result['form_action'][0] == "edit_status":
        try:
            sonde_id = str(result['sonde_id'][0])
            sonde_name = str(result['sonde_name'][0])
            sonde_lat = str(result['lat'][0])
            sonde_long = str(result['long'][0])
            sonde_email = str(result['email'][0])
            sonde_api_key = str(result['api_key'][0])
            status = str(result['status'][0])
            if status == "disable":
                status = str("0")
            else:
                status = str("1")
        except KeyError:
            pass
        output = change_sonde_status(sonde_name, status, sonde_lat, sonde_long, sonde_email, sonde_api_key, sonde_id)
    return render_template('admin_post.html', sonde_name=sonde_name, status=status, output=output)

if __name__ == '__main__':
    app.run(host=config['General']['listen_ip'], port=config['General']['listen_port'], threaded=config['General']['threaded'], debug=config['General']['debug'])
