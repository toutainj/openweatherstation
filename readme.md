# OpenWeatherStation

# Sondes

Voir le dossier [ESP8266-01](./ESP8266) pour le schéma de cablage et code

Outils recommandés :

  * [Thonny](https://thonny.org/)
  * [Firmware MicroPython pour ESP8266](http://micropython.org/download/esp8266/)
  * [ESPTool](https://github.com/espressif/esptool)


![Photo Sonde](./screenshots/proto_sonde.jpg)
![Photo Sonde 1](./screenshots/proto_sonde1.jpg)
![Photo Ecran SSD1306 et capteur temp/hum SI7021](./screenshots/proto_ssd1306_si7021.jpg)
![Photo Ecran SSD1306](./screenshots/proto_ssd1306.jpg)
![Schema cablage](./ESP8266/Schema/IOT_CESI_Schema_bb.jpg)

# API, Front, DB

![Schema Applicatif](./screenshots/schema_app.png)

<b>Fonctionnement</b>

Une base de donnée de donnée MariaDB/MySQL stocke les infos concernant les sondes (dont la geolocalisation pour affichage sur la carte) et les données relevées par des sondes conçues avec des microcontroleurs ESP8266-01 et capteur de température et humidité SI7021

Une API sert d'interface (HTTP POST) pour les sondes envoyant leurs données relevées via ESP8266/SI7021

Une documentation API (swagger via flask_restx) permet d'avoir une administration de l'ensemble sans passer par la base de donnée manuellement faire des requêtes SQL

Une interface Web (HTTP GET sur API) met en forme les données sous formes de tableau HTML avec les derniers relevés, les prévisions (API tierce openweathermap), les moyennes des derniers jours de données

## Screenshots WebUI Front

![Home](./screenshots/home.png)
![Carte](./screenshots/map.png)
![Graphique](./screenshots/graph.png)


## Installation de Docker sur debian buster

### Docker-ce
```bash
 apt-get install apt-transport-https ca-certificates curl gnupg2 software-properties-common
 curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
 apt-key fingerprint 0EBFCD88
 add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
 apt-get update
 apt-get install docker-ce
```

### Docker-compose
```bash
  curl -L "https://github.com/docker/compose/releases/download/1.28.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  chmod +x /usr/local/bin/docker-compose
  docker-compose --version
  ## si probleme avec la commande non inclus dans le path voir pour créer lien symbolique de ce type
  # ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

## Lancer le projet avec docker-compose
```bash
  docker-compose up -d
```

## CI / CD

### Setup gitlab-runner
```bash
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
export GITLAB_RUNNER_DISABLE_SKEL=true; sudo -E apt-get install gitlab-runner
sudo gitlab-runner register
```


# To DO

  * Finir documentation
  * Créer un système d'authentification (ajout table user (id, login, password) et modif table sonde pour lier à l'id utilisateur)
    * https://flask-login.readthedocs.io/en/latest/#configuring-your-application
    * https://www.digitalocean.com/community/tutorials/how-to-add-authentication-to-your-app-with-flask-login-fr
  * Implémenter une interface d'administration des sondes [In Progress]
  * Intégrer https://earth.nullschool.net/#current/wind/surface/level/orthographic=-357.64,50.27,3488
